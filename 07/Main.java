public class Main
{
  public static void main(String[] args) {
      
    int[] mass = new int[] {8, 4, 9, 5, 3};
    int n = 5;
    int y = 1;
    for(int x = n - 1; x > 0 && y > 0; x--) {
        y = 0;
        for(int i = 0 ; i < x; i++) { 
                if(mass[i] > mass[i+1]) {           
                    int mem = mass[i];
                    mass[i] = mass[i+1];
                    mass[i+1] = mem;
		    y = y + 1;
                }
            }
    }
        for(int j = 0; j < n; ++j) {
        System.out.println(mass[j]);
        }
  }
}
